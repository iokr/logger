package logger

import (
	"fmt"

	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

var Default = New(WithLevelAt(DebugLevel))

// ZapLogger .
type ZapLogger struct {
	opts options
	log  *zap.Logger
}

// New .
func New(opt ...Option) *ZapLogger {
	opts := defaultOptions
	for _, o := range opt {
		o.apply(&opts)
	}

	encoderConfig := zapcore.EncoderConfig{
		MessageKey:    opts.messageKey,
		LevelKey:      "level",
		TimeKey:       "time",
		StacktraceKey: "stacktrace",

		EncodeLevel:    zapcore.LowercaseLevelEncoder,
		EncodeTime:     zapcore.TimeEncoderOfLayout(opts.timeLayout),
		EncodeDuration: zapcore.StringDurationEncoder,
		EncodeCaller:   zapcore.ShortCallerEncoder,
	}

	var zapOptions []zap.Option
	if opts.addCaller {
		encoderConfig.CallerKey = "caller"
		zapOptions = append(zapOptions, zap.AddCaller())
		zapOptions = append(zapOptions, zap.AddCallerSkip(opts.callerSkip))
	}

	var encoder zapcore.Encoder
	switch opts.encoderType {
	case ConsoleEncoder:
		encoderConfig.ConsoleSeparator = " "
		encoder = zapcore.NewConsoleEncoder(encoderConfig)
	case JSONEncoder:
		encoder = zapcore.NewJSONEncoder(encoderConfig)
	}

	return &ZapLogger{
		opts: opts,
		log: zap.New(zapcore.NewCore(
			encoder,
			zapcore.AddSync(opts.output),
			zap.NewAtomicLevelAt(opts.level),
		), zapOptions...)}
}

func (z *ZapLogger) Sync() error {
	return z.log.Sync()
}

func (z *ZapLogger) Debug(msg string, fields ...Field) {
	z.log.Debug(msg, fields...)
}

func (z *ZapLogger) Debugf(format string, v ...interface{}) {
	z.log.Debug(fmt.Sprintf(format, v...))
}

func (z *ZapLogger) Info(msg string, fields ...Field) {
	z.log.Info(msg, fields...)
}

func (z *ZapLogger) Infof(format string, v ...interface{}) {
	z.log.Info(fmt.Sprintf(format, v...))
}

func (z *ZapLogger) Warn(msg string, fields ...Field) {
	z.log.Warn(msg, fields...)
}

func (z *ZapLogger) Warnf(format string, v ...interface{}) {
	z.log.Warn(fmt.Sprintf(format, v...))
}

func (z *ZapLogger) Error(msg string, fields ...Field) {
	z.log.Error(msg, fields...)
}

func (z *ZapLogger) Errorf(format string, v ...interface{}) {
	z.log.Error(fmt.Sprintf(format, v...))
}

func (z *ZapLogger) DPanic(msg string, fields ...Field) {
	z.log.DPanic(msg, fields...)
}

func (z *ZapLogger) DPanicf(format string, v ...interface{}) {
	z.log.DPanic(fmt.Sprintf(format, v...))
}

func (z *ZapLogger) Panic(msg string, fields ...Field) {
	z.log.Panic(msg, fields...)
}

func (z *ZapLogger) Panicf(format string, v ...interface{}) {
	z.log.Panic(fmt.Sprintf(format, v...))
}

func (z *ZapLogger) Fatal(msg string, fields ...Field) {
	z.log.Fatal(msg, fields...)
}

func (z *ZapLogger) Fatalf(format string, v ...interface{}) {
	z.log.Fatal(fmt.Sprintf(format, v...))
}

func Sync() error {
	if Default != nil {
		return Default.Sync()
	}
	return nil
}
