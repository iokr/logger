package logger

import (
	"io"
	"os"
)

type EncoderType string

const (
	ConsoleEncoder EncoderType = "console"
	JSONEncoder    EncoderType = "json"
)

const (
	defaultMessageKey         = "msg"
	defaultISO8601TimeEncoder = "2006-01-02T15:04:05.000Z0700"
)

type options struct {
	addCaller   bool
	callerSkip  int
	messageKey  string
	timeLayout  string
	level       Level
	encoderType EncoderType
	output      io.Writer
}

var defaultOptions = options{
	addCaller:   true,
	callerSkip:  1,
	messageKey:  defaultMessageKey,
	timeLayout:  defaultISO8601TimeEncoder,
	level:       InfoLevel,
	encoderType: ConsoleEncoder,
	output:      os.Stdout,
}

type Option interface {
	apply(*options)
}

type funcOption struct {
	f func(*options)
}

func (fdo *funcOption) apply(do *options) {
	fdo.f(do)
}

func newFuncOption(f func(*options)) *funcOption {
	return &funcOption{
		f: f,
	}
}

// WithMessageKey .
func WithMessageKey(messageKey string) Option {
	return newFuncOption(func(o *options) {
		o.messageKey = messageKey
	})
}

// WithEncoderType .
func WithEncoderType(encoderType EncoderType) Option {
	return newFuncOption(func(o *options) {
		o.encoderType = encoderType
	})
}

// WithOutput .
func WithOutput(output io.Writer) Option {
	return newFuncOption(func(o *options) {
		o.output = output
	})
}

// WithLevelAt .
func WithLevelAt(level Level) Option {
	return newFuncOption(func(o *options) {
		o.level = level
	})
}

// WithCaller .
func WithCaller(enabled bool) Option {
	return newFuncOption(func(o *options) {
		o.addCaller = enabled
	})
}

// WithCallerSkip .
func WithCallerSkip(skip int) Option {
	return newFuncOption(func(o *options) {
		o.callerSkip = skip
	})
}
