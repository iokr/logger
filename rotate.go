package logger

import (
	"io"

	"gopkg.in/natefinch/lumberjack.v2"
)

const (
	defaultFileName   = "./zap.log"
	defaultMaxSizeMB  = 500 // 文件最大500MB
	defaultMaxBackups = 10  // 保留10个数文件
	defaultMaxAgeDay  = 7   // 最多保存7天
)

type RotateOption func(*Rotate)

// WithFileName set log file name.
//  default zap.log.
func WithFileName(filename string) RotateOption {
	return func(rotate *Rotate) {
		rotate.filename = filename
	}
}

// WithMaxSize set log file max size unit M.
//  default 500M.
func WithMaxSize(maxSize int) RotateOption {
	return func(rotate *Rotate) {
		rotate.maxSize = maxSize
	}
}

// WithMaxBackups set log file max backups.
//  default 10.
func WithMaxBackups(maxBackups int) RotateOption {
	return func(rotate *Rotate) {
		rotate.maxBackups = maxBackups
	}
}

// WithMaxAge set log file max age.
//  default 7 day.
func WithMaxAge(maxAge int) RotateOption {
	return func(rotate *Rotate) {
		rotate.maxAge = maxAge
	}
}

// WithLocalTime set log file name suffix.
//  default false.
func WithLocalTime(isLocalTime bool) RotateOption {
	return func(rotate *Rotate) {
		rotate.localTime = isLocalTime
	}
}

// WithCompress set log file backup is compress.
//  default false.
func WithCompress(isCompress bool) RotateOption {
	return func(rotate *Rotate) {
		rotate.compress = isCompress
	}
}

// Rotate .
type Rotate struct {
	filename   string // 文件路径
	maxSize    int    // 文件最大M数,超过则切割
	maxBackups int    // 最大文件保留数，超过就删除最老的日志文件
	maxAge     int    // 最大保存天数
	localTime  bool   // 当地时间
	compress   bool   // 是否压缩
}

// NewRotate .
func NewRotate(opts ...RotateOption) *Rotate {
	rotate := &Rotate{
		filename:   defaultFileName,
		maxSize:    defaultMaxSizeMB,
		maxBackups: defaultMaxBackups,
		maxAge:     defaultMaxAgeDay,
		localTime:  false,
		compress:   false,
	}

	for _, opt := range opts {
		opt(rotate)
	}
	return rotate
}

// Writer .
func (r *Rotate) Writer() io.Writer {
	return &lumberjack.Logger{
		Filename:   r.filename,
		MaxSize:    r.maxSize,
		MaxBackups: r.maxBackups,
		MaxAge:     r.maxAge,
		LocalTime:  r.localTime,
		Compress:   r.compress,
	}
}
